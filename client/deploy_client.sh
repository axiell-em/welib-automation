#!/usr/bin/env bash

######################## Script properties ########################
usage() { echo "Usage: $0 [-u user] [-r remote repo] [-b branch name] [-h host address] [-d virtual host domain name] [-i installation dir] [-c optional config]" 1>&2; exit 1; }

while getopts ":u:r:b:h:d:i:c:" option
do
case "${option}"
in
u) USER=${OPTARG};;
r) REPO=${OPTARG};;
b) BRANCH=${OPTARG};;
h) HOST=$OPTARG;;
d) DOMAIN=$OPTARG;;
i) INST=${OPTARG};;
c) CONF=$OPTARG;;
h) usage;;
*) usage;;
esac
done
shift $((OPTIND-1))

if [ -z "${USER}" ] || [ -z "${REPO}" ] || [ -z "${BRANCH}" ] || [ -z "${HOST}" ] || [ -z "${DOMAIN}" ] || [ -z "${INST}" ] || [ -z "${CONF}" ] ; then
    usage
fi

echo "user: $USER"
echo "remote: $REPO"
echo "branch: $BRANCH"
echo "host: $HOST"
echo "domain: $DOMAIN"
echo "inst: $INST"
echo "conf: $CONF"

######################## Pre build scripts ########################
#Cleanup current client app repo
rm -rf welib-frontend
#Checkout the given branch (feature, develop, release or master)
echo "git clone -b ${BRANCH} ${REPO}"
git clone -b ${BRANCH} ${REPO}
#Change dir to the freshly cloned repo
cd ./welib-frontend

########################## build scripts ##########################
#Build the current client app
echo "npm install && gulp templates && gulp buildProd"
npm install && gulp templates && gulp buildProd

######################## Post build scripts #######################
echo "ssh -t ${USER}@${HOST} sudo rm -rf /tmp/dist"
ssh -t ${USER}@${HOST} sudo rm -rf /tmp/dist
echo "scp -r dist/ ${USER}@${HOST}:/tmp"
scp -r dist/ ${USER}@${HOST}:/tmp
#Backup the current setup
TODAY=`date +%Y-%m-%d`
BKP="${INST}_${TODAY}"
echo "ssh -t ${USER}@${HOST} sudo mkdir -p /tmp/backups/${BKP}"
ssh -t ${USER}@${HOST} sudo mkdir -p /tmp/backups/${BKP}
echo "ssh -t ${USER}@${HOST} sudo cp -rf /var/www/shared-client/${INST}/* /tmp/backups/${BKP}"
ssh -t ${USER}@${HOST} sudo cp -rf /var/www/shared-client/${INST}/* /tmp/backups/${BKP}
#Copy the new dist to the target client installation
echo "ssh -t ${USER}@${HOST} sudo cp -rf /tmp/dist/* /var/www/shared-client/${INST}"
ssh -t ${USER}@${HOST} sudo cp -rf /tmp/dist/* /var/www/shared-client/${INST}

#Replace the config if it's needed
if [[ ${CONF} ]]; then
#Backup
echo "ssh -t ${USER}@${HOST} sudo cp -f /var/www/${DOMAIN}/public_html/customer.conf /var/www/${DOMAIN}/public_html/customer.conf.bk"
ssh -t ${USER}@${HOST} sudo cp -f /var/www/${DOMAIN}/public_html/customer.conf /var/www/${DOMAIN}/public_html/customer.conf.bk
#Replace
echo "ssh -t ${USER}@${HOST} sudo cp -f /var/www/shared-client/${INST}/config/${CONF} /var/www/${DOMAIN}/public_html/customer.conf"
ssh -t ${USER}@${HOST} sudo cp -f /var/www/shared-client/${INST}/config/${CONF} /var/www/${DOMAIN}/public_html/customer.conf
fi








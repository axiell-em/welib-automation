## automation

A collection of scripts for automating the manual deployment for our WeLib platform as a first step towards a fully functional CDI, it includes the following:

1. Client app deploy

2. Service stack deploy (TODO)

---

#### Client app script

**Usage** 
```bash
Ashrafs-MacBook-Pro:client assar$ ./deploy_client.sh -h
Usage: ./deploy_client.sh [-u user] [-r remote repo] [-b branch name] 
                          [-h host address] [-d virtual host domain name]
                          [-i installation dir] [-c optional config]
                          [-h optional help]
```
**Example** 
```bash
Ashrafs-MacBook-Pro:client assar$ ./deploy_client.sh -u assar 
                                                     -r git@bitbucket.org:axiell-em/welib-frontend.git 
                                                     -b develop 
                                                     -h 10.94.17.12 
                                                     -d dev.welib.se 
                                                     -i current-develop-new 
                                                     -c dev.conf
```


